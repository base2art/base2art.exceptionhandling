﻿namespace Base2art.ExceptionHandling
{
    using System;

    public class ExceptionHandledEventArgs : EventArgs
    {
        public ExceptionHandledEventArgs() : this(false)
        {
        }

        public ExceptionHandledEventArgs(bool defaultHandledValue)
            => this.Handled = defaultHandledValue;

        public bool Handled { get; set; }
    }
}