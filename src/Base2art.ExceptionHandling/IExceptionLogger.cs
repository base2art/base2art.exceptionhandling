﻿namespace Base2art.ExceptionHandling
{
    using System;
    using System.Threading.Tasks;

    public interface IExceptionLogger
    {
        Task HandleException(Exception e);
    }
}