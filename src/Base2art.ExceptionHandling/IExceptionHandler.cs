﻿namespace Base2art.ExceptionHandling
{
    using System;
    using System.Threading.Tasks;

    public interface IExceptionHandler
    {
        Task HandleException(Exception e, ExceptionHandledEventArgs args);
    }
}